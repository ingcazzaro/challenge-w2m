# Getting Started

SuperHero CHallenge

### RUN IT WITH DOCKER

* docker image build -t challenge-fc .
* docker container run -p 8080:8080 challenge-fc

### TEST IT WITH SWAGGER

- User: test / Password: test

* Go to [Online Documentation](http://localhost:8080/swagger-ui.html#/)

### TEST IT WITH CURL EXAMPLE

curl -X GET "http://localhost:8080/superheroes" -H  "accept: */*" -u test:test

Author: Facundo Cazzaro (ingcazzaro@gmail.com)

### WATCH REQUEST/RESPONSE INFO

Requests/responses are logged with aspects. To see this info please set up the log level to DEBUG (
logging.level.com.w2m.challenge=DEBUG)