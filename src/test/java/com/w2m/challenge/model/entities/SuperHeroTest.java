package com.w2m.challenge.model.entities;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class SuperHeroTest {

	@Test
	void creteHeroOk() {
		SuperHero superHero = SuperHero.builder()
				.name("Spiderman")
				.power(70)
				.defense(50)
				.build();

		assertEquals("Spiderman", superHero.getName());
		assertEquals((Integer) 70, superHero.getPower());
		assertEquals((Integer) 50, superHero.getDefense());
	}

}