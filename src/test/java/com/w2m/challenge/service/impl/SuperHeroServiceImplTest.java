package com.w2m.challenge.service.impl;

import com.w2m.challenge.model.entities.SuperHero;
import com.w2m.challenge.repository.SuperHeroRepository;
import org.dozer.DozerBeanMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SuperHeroServiceImplTest {
    @Mock
    private SuperHeroRepository superHeroRepository;
    @Mock
    private DozerBeanMapper mapper;

    @InjectMocks
    private SuperHeroServiceImpl superHeroService;


    @Test
    void getAll() {
        superHeroService.getAll();
        verify(superHeroRepository, times(1)).findAll();
    }

    @Test
    void getById() {
        final SuperHero superHero = SuperHero.builder().build();
        when(superHeroRepository.findById(any(Long.class))).thenReturn(Optional.of(superHero));

        superHeroService.getById(1L);
        verify(superHeroRepository, times(1)).findById(any(Long.class));
    }

    @Test
    void getAllByName() {
        superHeroService.getAllByName("man");
        //Updated using stream
        //verify(superHeroRepository, times(1)).findAllByNameContainingIgnoreCase(any(String.class));
        verify(superHeroRepository, times(1)).findAll();
    }

    @Test
    void deleteById() {
        final SuperHero superHero = SuperHero.builder().build();
        when(superHeroRepository.findById(any(Long.class))).thenReturn(Optional.of(superHero));

        superHeroService.deleteById(2L);

        verify(superHeroRepository, times(1)).findById(any(Long.class));
        verify(superHeroRepository, times(1)).delete(any(SuperHero.class));
    }
}