package com.w2m.challenge.controller;

import com.w2m.challenge.model.entities.SuperHero;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SuperHeroControllerTest {
    private static final Logger log = LoggerFactory.getLogger(SuperHeroControllerTest.class);
    private final TestRestTemplate restTemplate = new TestRestTemplate().withBasicAuth("test", "test");

    @Value("${base.url}")
    private String baseUrl;

    @LocalServerPort
    int serverPort;

    @Test
    void testGetAllSuperHerosSuccess() {
        URI uri = getUriWith("/superheroes");

        ResponseEntity<List> result = restTemplate.getForEntity(uri, List.class);

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(4, Objects.requireNonNull(result.getBody()).size());
    }

    @Test
    void testGetSuperHeroByIdSuccess() {
        URI uri = getUriWith("/superheroes/1");

        ResponseEntity<SuperHero> result = restTemplate.getForEntity(uri, SuperHero.class);

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(Long.valueOf(1), Objects.requireNonNull(result.getBody()).getId());
    }

    @Test
    void testGetSuperHeroByNameSuccess() {
        URI uri = getUriWith("/superheroes/search?name=man");

        ResponseEntity<List> result = restTemplate.getForEntity(uri, List.class);

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(2, Objects.requireNonNull(result.getBody()).size());
    }

    @Test
    void testModSuperHeroByIdSuccess() {
        URI uri = getUriWith("/superheroes/1");
        ResponseEntity<SuperHero> original = restTemplate.getForEntity(uri, SuperHero.class);

        SuperHero hero = SuperHero.builder().name("newName").build();
        HttpEntity<SuperHero> requestUpdate = new HttpEntity<>(hero);
        ResponseEntity<SuperHero> result = restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, SuperHero.class);

        assertEquals(200, result.getStatusCodeValue());
        assertEquals("newName", Objects.requireNonNull(result.getBody()).getName());
        assertEquals(Objects.requireNonNull(original.getBody()).getDefense(), result.getBody().getDefense());
    }

    @Test
    void testDeleteSuperHeroByIdSuccess() {
        URI uri = getUriWith("/superheroes/1");
        restTemplate.delete(uri);

        restTemplate.getForEntity(uri, SuperHero.class);
    }

    private URI getUriWith(String path) {
        URI uri = null;

        try {
            uri = new URI(baseUrl + ":" + serverPort + path);
        } catch (URISyntaxException e) {
            log.error("BAD Url");
            e.printStackTrace();
        }

        return uri;
    }

}