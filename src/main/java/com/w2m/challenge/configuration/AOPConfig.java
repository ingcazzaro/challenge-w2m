package com.w2m.challenge.configuration;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy
public class AOPConfig {
}
