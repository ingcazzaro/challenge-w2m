package com.w2m.challenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

@ControllerAdvice
public class GlobalExceptionHandler {
	HandlerResponse handlerResponse;

	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<HandlerResponse> noSuchElementException(NoSuchElementException e) {
		handlerResponse = new HandlerResponse(LocalDateTime.now(), "Resource not found: " + e.getMessage(), HttpStatus.NOT_FOUND, "Error");
		return new ResponseEntity<>(handlerResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<HandlerResponse> runtimeException(RuntimeException e) {
		handlerResponse = new HandlerResponse(LocalDateTime.now(), "Exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, "Error");
		return new ResponseEntity<>(handlerResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<HandlerResponse> exception(Exception e) {
		handlerResponse = new HandlerResponse(LocalDateTime.now(), "Exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, "Error");
		return new ResponseEntity<>(handlerResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
