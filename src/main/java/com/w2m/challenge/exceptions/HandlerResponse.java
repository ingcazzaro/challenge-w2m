package com.w2m.challenge.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HandlerResponse {
	private LocalDateTime timestamp;
	private String data;
	private HttpStatus respondeCode;
	private String status;
}
