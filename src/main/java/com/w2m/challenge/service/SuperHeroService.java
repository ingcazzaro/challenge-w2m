package com.w2m.challenge.service;

import com.w2m.challenge.model.dto.SuperHeroDTO;
import com.w2m.challenge.model.entities.SuperHero;

import java.util.List;

public interface SuperHeroService {
    List<SuperHero> getAll();

    SuperHero getById(Long id);

    List<SuperHero> getAllByName(String name);

    SuperHero updateById(Long id, SuperHeroDTO updatedSuperHero);

    String deleteById(Long id);
}
