package com.w2m.challenge.service.impl;

import com.w2m.challenge.model.dto.SuperHeroDTO;
import com.w2m.challenge.model.entities.SuperHero;
import com.w2m.challenge.repository.SuperHeroRepository;
import com.w2m.challenge.service.SuperHeroService;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SuperHeroServiceImpl implements SuperHeroService {
    private final SuperHeroRepository superHeroRepository;
    private DozerBeanMapper mapper;

    public SuperHeroServiceImpl(SuperHeroRepository superHeroRepository) {
        this.superHeroRepository = superHeroRepository;
    }

    @PostConstruct
    private void init() {
        mapper = new DozerBeanMapper();

        mapper.addMapping(new BeanMappingBuilder() {
            protected void configure() {
                mapping(SuperHeroDTO.class, SuperHero.class, TypeMappingOptions.mapNull(false));
            }
        });
    }

    @Override
    public List<SuperHero> getAll() {
        return superHeroRepository.findAll();
    }

    @Override
    public SuperHero getById(Long id) {
        return superHeroRepository.findById(id).orElseThrow();
    }

    @Override
    public List<SuperHero> getAllByName(String name) {
        //I'll replace this call to use streams.
        //return superHeroRepository.findAllByNameContainingIgnoreCase(name);

        return superHeroRepository.findAll().stream()
                .filter(superHero -> superHero.getName()
                        .toLowerCase()
                        .contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    public SuperHero updateById(Long id, SuperHeroDTO updatedSuperHero) {
        SuperHero superHero = superHeroRepository.findById(id).orElseThrow();
        mapper.map(updatedSuperHero, superHero);

        return superHeroRepository.save(superHero);
    }

    @Override
    public String deleteById(Long id) {
        SuperHero superHero = superHeroRepository.findById(id).orElseThrow();

        superHeroRepository.delete(superHero);
        return String.format("SuperHero id: %d deleted", id);
    }
}
