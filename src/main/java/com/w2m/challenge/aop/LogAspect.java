package com.w2m.challenge.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Aspect
@Component
@Slf4j
public class LogAspect {
    public static final String LINE = "==============================================================";

    @Before("execution(public * com.w2m.challenge.controller.*.*(..))")
    public void logBefore(JoinPoint joinPoint) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        log.debug(LINE);
        log.debug("URL = {} - IP = {}", request.getRequestURL(), request.getRemoteAddr());
        log.debug("METHOD = {} - CLASS_METHOD = {}", request.getMethod(), joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.debug("ARGS = {}", joinPoint.getArgs());
        log.debug("REQUEST TIME = {}", LocalDateTime.now());
        log.debug(LINE);
    }

    @AfterReturning(value = "execution(public * com.w2m.challenge.controller.*.*(..))", returning = "object")
    public void logAfterReturn(Object object) {
        log.debug(LINE);
        log.debug("RESPONSE = {}", object.toString());
        log.debug(LINE);
    }

    @Around("@annotation(LogExecutionTime)")
    public Object measureExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long executionTime = System.currentTimeMillis() - startTime;

        log.info("Method {} was executed in {} ms", joinPoint.getSignature().getName(), executionTime);
        return proceed;
    }
}