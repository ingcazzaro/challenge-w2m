package com.w2m.challenge.controller;

import com.w2m.challenge.aop.LogExecutionTime;
import com.w2m.challenge.model.dto.SuperHeroDTO;
import com.w2m.challenge.model.entities.SuperHero;
import com.w2m.challenge.service.SuperHeroService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/superheroes")
@Slf4j
public class SuperHeroController {
	private final SuperHeroService superHeroService;

	public SuperHeroController(SuperHeroService superHeroService) {
		this.superHeroService = superHeroService;
	}

	@Cacheable(value = "allSuperHeroesCache")
	@LogExecutionTime
	@GetMapping
	public List<SuperHero> getAllSuperHeroes() {
		log.info("List all superheroes");
		return superHeroService.getAll();
	}

	@LogExecutionTime
	@GetMapping("/{id}")
	public SuperHero getSuperHeroById(@PathVariable Long id) {
		log.info("Get superhero id: {}", id);
		return superHeroService.getById(id);
	}

	@LogExecutionTime
	@GetMapping("/search")
	public List<SuperHero> getSuperHeroByName(@RequestParam String name) {
		log.info("Listing all superheroes containing '{}' in name", name);
		return superHeroService.getAllByName(name);
	}

	@PutMapping("/{id}")
	public SuperHero getModSuperHeroById(@PathVariable Long id, @RequestBody SuperHeroDTO updatedSuperHero) {
		log.info("Update superhero id: {}", id);
		return superHeroService.updateById(id, updatedSuperHero);
	}

	@DeleteMapping("/{id}")
	public String deleteSuperHeroById(@PathVariable Long id) {
		log.info("Delete superhero id: {}", id);
		return superHeroService.deleteById(id);
	}
}
