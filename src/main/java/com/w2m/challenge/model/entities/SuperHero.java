package com.w2m.challenge.model.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Entity
@Table(name = "superhero")
public class SuperHero implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 100, unique = true, nullable = false)
	private String name;

	@Column(nullable = false)
	private Integer power;

	@Column(nullable = false)
	private Integer defense;

}
