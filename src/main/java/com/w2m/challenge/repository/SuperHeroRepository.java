package com.w2m.challenge.repository;

import com.w2m.challenge.model.entities.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SuperHeroRepository extends JpaRepository<SuperHero, Long> {
	List<SuperHero> findAllByNameContainingIgnoreCase(String name);

}
