#Build
FROM maven:3.8.1-openjdk-11-slim AS MAVEN_BUILD
MAINTAINER Facundo Cazzaro

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/
RUN mvn package

#Run
FROM openjdk:12-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/challenge-0.0.2-SNAPSHOT.jar /app/

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "challenge-0.0.2-SNAPSHOT.jar"]